function calcular() {
  let a;
  let b;
  let c;

  a = document.getElementById("numero1").value;
  b = document.getElementById("numero2").value;

  a = parseInt(a);
  b = parseInt(b);

  c = mayor(a, b);

  multiplicar(c);
}

function multiplicar(a) {
  let i; //Variable tipo contador la idea es que el valor inicial de i sea 1 y el valor final sea 10

  i = 1;

  //Ciclo

  while (i <= 10) {
    console.log(a * i);
    i++;
  }
}

function mayor(a, b) {
  let resultado;

  if (b > a) {
    resultado = b;
  } else {
    resultado = a;
  }

  alert(resultado);
  console.log("El número mayor es: "+resultado);

  return resultado;
}
